<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\barang;

class MainController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }
    public function about()
    {
        return view('about');
    }
    public function shop()
    {
        $barang = barang::all();
        return view('shop');
    }
    public function detail()
    {
        return view('detail');
    }
    public function cart()
    {
        return view('cart');
    }
    public function checkout()
    {
        return view('checkout');
    }
    public function contact()
    {
        return view('contact');
    }
    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id_kategori' => 'required',
            'nama' => 'required',
            'harga' => 'required',
            'ukuran' => 'required'
        ]);

        barang::create([
            'id_kategori' => $request->id_kategori,
            'nama' => $request->nama,
            'harga' => $request->harga,
            'ukuran' => $request->ukuran
        ]);

        return redirect('/shop');
    }
}
