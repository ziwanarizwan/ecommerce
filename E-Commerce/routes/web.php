<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'MainController@welcome');
Route::get('/about', 'MainController@about');
Route::get('/shop', 'MainController@shop');
Route::get('/cart', 'MainController@cart');
Route::get('/checkout', 'MainController@checkout');
Route::get('/contact', 'MainController@contact');

Route::get('/shop/create', 'MainController@create');
Route::post('/shop', 'MainController@store');
Route::get('/detail/{barang_id}', 'MainController@detail');
Route::get('/detail/{barang_id}/edit', 'MainController@edit');
Route::put('/detail/{barang_id}', 'MainController@update');
Route::delete('/detail/{barang_id}', 'MainController@destroy');

Route::get('/register', 'AuthController@register');
