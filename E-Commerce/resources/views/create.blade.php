<h2>Tambah Data</h2>
<form action="/shop" method="POST">
    @csrf
    <div class="form-group">
        <label for="kategori">kategori</label>
        <input type="text" class="form-control" name="id_kategori" id="id_kategori" placeholder="Masukkan Id Kategori">
        @error('id_kategori')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="nama">nama</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="harga">harga</label>
        <input type="text" class="form-control" name="harga" id="harga" placeholder="Masukkan Harga">
        @error('harga')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="ukuran">ukuran</label>
        <input type="text" class="form-control" name="ukuran" id="ukuran" placeholder="Masukkan ukuran">
        @error('ukuran')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>